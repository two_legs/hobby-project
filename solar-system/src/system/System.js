import Sun from "./sun/Sun";

/**
 * Renders the solar system
 */
class System {
  /**
   *
   * @param {
   *   config: solar system config,
   *   scene: the scene to render the system into
   * }
   */
  constructor({ config, scene }) {
    this.sun = new Sun({
      config: config.sun,
      scene
    });
  }
}

export default System;
