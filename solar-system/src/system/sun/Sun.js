import { Mesh } from "@babylonjs/core/Meshes/mesh";
import { GridMaterial } from "@babylonjs/materials/grid";

/**
 * You are, the sun
 */
class Sun {
  constructor({ config, scene }) {
    const { name } = config;

    // Our built-in 'sphere' shape. Params: name, subdivs, size, scene
    this.sphere = Mesh.CreateSphere(name, 16, 2, scene);
    this.sphere.material = new GridMaterial("grid", scene);
  }
}

export default Sun;
